function arr(num) {
    let array = [];
    for (let i = 0; i < num; i++) {
        let n = Math.ceil(Math.random() * 100);
        array.push(n);
    }

    return radix(array, 0, null);
}

function radix(array, index, to) {
    if (index === to) {
        return array;
    }

    let max;
    let sorted = [[], [], [], [], [], [], [], [], [], []];

    for (let i = 0; i < array.length; i++) {
        let numero = array[i].toString();
        if (numero.length < (index + 1)) {
            sorted[0].push(array[i]);
            continue;
        }
        let minux = numero.charAt(numero.length - (index + 1));

        sorted[parseInt(minux)].push(array[i]);
        if (!to && (max === undefined || array[i] > max)) {
            max = array[i];
        }
    }

    if (max != undefined) {
        to = max.toString().length;
    }

    array = [];
    for (let l = 0; l < 10; l++) {
        array = [...array, ...sorted[l]];
    }

    return radix(array, ++index, 12);
}

console.log(radix(arr(16), 0, null));