define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./manual/main.js",
    "group": "C__Users_Asus_Desktop_Salvacion_manual_main_js",
    "groupTitle": "C__Users_Asus_Desktop_Salvacion_manual_main_js",
    "name": ""
  },
  {
    "type": "post",
    "url": "/clients",
    "title": "Create a new client",
    "name": "CreateClient",
    "group": "Clients",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory - the name of client</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Mandatory - the last name of client</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "RUT",
            "description": "<p>Mandatory - the identifier of client</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "birth_date",
            "description": "<p>Mandatory - the birth date of client</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phone",
            "description": "<p>Mandatory - the phone of client</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "adress",
            "description": "<p>Mandatory - the adress date of client</p>"
          },
          {
            "group": "Parameter",
            "type": "Char",
            "optional": false,
            "field": "genre",
            "description": "<p>Mandatory - the genre of client</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "BAD_REQUEST",
            "description": "<p>dont have anythings</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>client created.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.RUT",
            "description": "<p>RUT of created client.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.name",
            "description": "<p>name of created client.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.last_name",
            "description": "<p>last name of created client.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.birth_date",
            "description": "<p>birth date of created client.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.phone",
            "description": "<p>phone of created client.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.adress",
            "description": "<p>adress of created client.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.genre",
            "description": "<p>genre of created client.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Clients.js",
    "groupTitle": "Clients"
  },
  {
    "type": "delete",
    "url": "/clients/rut/:RUT",
    "title": "Delete an client with a specify RUT.",
    "name": "DeleteClient",
    "group": "Clients",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "RUT",
            "description": "<p>Mandatory - the RUT of client you want to delete.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "204": [
          {
            "group": "204",
            "type": "VoRUT",
            "optional": false,
            "field": "response",
            "description": "<p>The voRUT if the action works.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Clients.js",
    "groupTitle": "Clients"
  },
  {
    "type": "get",
    "url": "/clients",
    "title": "Get the list of clients",
    "name": "GetClient",
    "group": "Clients",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "response",
            "description": "<p>A list of clients.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.RUT",
            "description": "<p>RUT of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.name",
            "description": "<p>name of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.last_name",
            "description": "<p>last name of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.birth_date",
            "description": "<p>birth date of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.phone",
            "description": "<p>phone of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.adress",
            "description": "<p>adress of created client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.genre",
            "description": "<p>genre of created client.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Clients.js",
    "groupTitle": "Clients"
  },
  {
    "type": "get",
    "url": "/clients/rut/:RUT",
    "title": "Get an client with a specify identifier.",
    "name": "GetClient",
    "group": "Clients",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "RUT",
            "description": "<p>Mandatory - the identifier of client you want to get.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>A list of clients.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.RUT",
            "description": "<p>RUT of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.name",
            "description": "<p>name of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.last_name",
            "description": "<p>last name of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.birth",
            "description": "<p>date birth_date of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.phone",
            "description": "<p>phone of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.adress",
            "description": "<p>adress of created client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.genre",
            "description": "<p>genre of created client.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Clients.js",
    "groupTitle": "Clients"
  },
  {
    "type": "get",
    "url": "/clients/lastname/:last_name",
    "title": "Get an client with a specify last name.",
    "name": "GetClient",
    "group": "Clients",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Mandatory - the last name of client you want to get.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>A list of clients.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.RUT",
            "description": "<p>RUT of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.name",
            "description": "<p>name of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.last_name",
            "description": "<p>last name of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.birth_date",
            "description": "<p>birth date of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.phone",
            "description": "<p>phone of client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.adress",
            "description": "<p>adress of created client.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.genre",
            "description": "<p>genre of created client.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Clients.js",
    "groupTitle": "Clients"
  },
  {
    "type": "patch",
    "url": "/clients/rut/:RUT",
    "title": "Update an client with a specify identifier.",
    "name": "UpdateClient",
    "group": "Clients",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "RUT",
            "description": "<p>Mandatory - the RUT of client you want to update</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory - the name of client</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Mandatory - the last name of client</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "birth_date",
            "description": "<p>Mandatory - the birth date of client</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phone",
            "description": "<p>Mandatory - the phone of client</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "adress",
            "description": "<p>Mandatory - the adress date of client</p>"
          },
          {
            "group": "Parameter",
            "type": "Char",
            "optional": false,
            "field": "genre",
            "description": "<p>Mandatory - the genre of client</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "BAD_REQUEST",
            "description": "<p>dont have anythings</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "204": [
          {
            "group": "204",
            "type": "VoRUT",
            "optional": false,
            "field": "response",
            "description": "<p>The voRUT if the action works.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Clients.js",
    "groupTitle": "Clients"
  },
  {
    "type": "post",
    "url": "/plans",
    "title": "Create a new plan",
    "name": "CreatePlan",
    "group": "Plans",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory - the name of plan</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "trans_speed",
            "description": "<p>Mandatory - the trans speed of plan</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "plan_type",
            "description": "<p>Mandatory - the plan type of plan</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "promotion",
            "description": "<p>Mandatory - the promotion of plan</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>Mandatory - the value of plan</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "BAD_REQUEST",
            "description": "<p>dont have anythings</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>plan created.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.name",
            "description": "<p>name of created plan.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.trans_speed",
            "description": "<p>trans speed of created plan.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.plan_type",
            "description": "<p>plan type of created plan.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.promotion",
            "description": "<p>promotion of created plan.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.value",
            "description": "<p>value of created plan.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Plans.js",
    "groupTitle": "Plans"
  },
  {
    "type": "delete",
    "url": "/plans/:name",
    "title": "Delete an plan with a specify name.",
    "name": "DeletePlan",
    "group": "Plans",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory - the name of plan you want to delete.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "204": [
          {
            "group": "204",
            "type": "Voname",
            "optional": false,
            "field": "response",
            "description": "<p>The voname if the action works.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Plans.js",
    "groupTitle": "Plans"
  },
  {
    "type": "get",
    "url": "/plans",
    "title": "Get the list of plans",
    "name": "GetPlan",
    "group": "Plans",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "response",
            "description": "<p>A list of plans.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.name",
            "description": "<p>name of plan.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.trans_speed",
            "description": "<p>trans speed of plan.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.plan_type",
            "description": "<p>plan type of plan.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.promotion",
            "description": "<p>promotion of plan.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.value",
            "description": "<p>value of plan.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Plans.js",
    "groupTitle": "Plans"
  },
  {
    "type": "get",
    "url": "/plans/:name",
    "title": "Get an plan with a specify name.",
    "name": "GetPlan",
    "group": "Plans",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory - the name of plan you want to get.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>A list of plans.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.name",
            "description": "<p>name of plan.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.trans_speed",
            "description": "<p>trans speed of plan.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.plan_type",
            "description": "<p>plan type of plan.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.promotion",
            "description": "<p>promotion of plan.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.value",
            "description": "<p>value of plan.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Plans.js",
    "groupTitle": "Plans"
  },
  {
    "type": "patch",
    "url": "/plans/:name",
    "title": "Update an plan with a specify name.",
    "name": "UpdatePlan",
    "group": "Plans",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory - the name of plan you want to update.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "trans_speed",
            "description": "<p>Mandatory - the trans speed of plan</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "plan_type",
            "description": "<p>Mandatory - the plan type of plan</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "promotion",
            "description": "<p>Mandatory - the promotion of plan</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>Mandatory - the value of plan</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "BAD_REQUEST",
            "description": "<p>dont have anythings</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "204": [
          {
            "group": "204",
            "type": "VoName",
            "optional": false,
            "field": "response",
            "description": "<p>The voname if the action works.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Plans.js",
    "groupTitle": "Plans"
  },
  {
    "type": "post",
    "url": "/associations",
    "title": "Create a new association",
    "name": "CreateAssociation",
    "group": "associations",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "ClientRUT",
            "optional": false,
            "field": "client",
            "description": "<p>Mandatory - the client of association</p>"
          },
          {
            "group": "Parameter",
            "type": "PlanName",
            "optional": false,
            "field": "plan",
            "description": "<p>Mandatory - the plan of association</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "assign",
            "description": "<p>Mandatory - the assign of association</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "adress",
            "description": "<p>Mandatory - the adress of association</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "begin_date",
            "description": "<p>Mandatory - the begin date of association</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "BAD_REQUEST",
            "description": "<p>dont have anythings</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>association created.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.client",
            "description": "<p>client of created association.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.plan",
            "description": "<p>plan of created association.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.assign",
            "description": "<p>assign of created association.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.adress",
            "description": "<p>adress of created association.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.begin_date",
            "description": "<p>begin date of created association.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Associations.js",
    "groupTitle": "associations"
  },
  {
    "type": "delete",
    "url": "/associations/:assign",
    "title": "Delete an association with a specify assign.",
    "name": "Deleteassociation",
    "group": "associations",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "assign",
            "description": "<p>Mandatory - the assign of association you want to delete.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "204": [
          {
            "group": "204",
            "type": "Voassign",
            "optional": false,
            "field": "response",
            "description": "<p>The voassign if the action works.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Associations.js",
    "groupTitle": "associations"
  },
  {
    "type": "get",
    "url": "/associations",
    "title": "Get the list of associations",
    "name": "GetAssociation",
    "group": "associations",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "response",
            "description": "<p>A list of associations.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.client",
            "description": "<p>client of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.plan",
            "description": "<p>plan of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.assign",
            "description": "<p>assign of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.adress",
            "description": "<p>adress of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.begin_date",
            "description": "<p>begin date of association.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Associations.js",
    "groupTitle": "associations"
  },
  {
    "type": "get",
    "url": "/associations/client/:clientRUT",
    "title": "Get an association with a specify client.",
    "name": "GetAssociation",
    "group": "associations",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "RUT",
            "description": "<p>Mandatory - the RUT of client you want to get.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>A list of associations.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.client",
            "description": "<p>client of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.plan",
            "description": "<p>plan of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.assign",
            "description": "<p>assign of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.adress",
            "description": "<p>adress of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.begin_date",
            "description": "<p>begin date of association.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Associations.js",
    "groupTitle": "associations"
  },
  {
    "type": "get",
    "url": "/associations/plan/:planName",
    "title": "Get an association with a specify plan.",
    "name": "GetAssociation",
    "group": "associations",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory - the name of the plan you want to get.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>A list of associations.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.client",
            "description": "<p>client of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.plan",
            "description": "<p>plan of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.assign",
            "description": "<p>assign of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.adress",
            "description": "<p>adress of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.begin_date",
            "description": "<p>begin date of association.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Associations.js",
    "groupTitle": "associations"
  },
  {
    "type": "get",
    "url": "/associations/:assign",
    "title": "Get an association with a specify assign.",
    "name": "GetAssociation",
    "group": "associations",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "assign",
            "description": "<p>Mandatory - the assign of association you want to get.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>A list of associations.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.client",
            "description": "<p>client of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.plan",
            "description": "<p>plan of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.assign",
            "description": "<p>assign of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.adress",
            "description": "<p>adress of association.</p>"
          },
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response.begin_date",
            "description": "<p>begin date of association.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Associations.js",
    "groupTitle": "associations"
  },
  {
    "type": "patch",
    "url": "/associations/:assign",
    "title": "Update an association with a specify assign.",
    "name": "Updateassociation",
    "group": "associations",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "assign",
            "description": "<p>Mandatory - the assign of association you want to update.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "204": [
          {
            "group": "204",
            "type": "Voassign",
            "optional": false,
            "field": "response",
            "description": "<p>The voassign if the action works.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Associations.js",
    "groupTitle": "associations"
  },
  {
    "type": "post",
    "url": "/billingss",
    "title": "Create a new billings",
    "name": "CreateBillings",
    "group": "billingss",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>Mandatory - the client of billings</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "plan",
            "description": "<p>Mandatory - the plan of billings</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "month",
            "description": "<p>Mandatory - the month of billings</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "year",
            "description": "<p>Mandatory - the year of billings</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>Mandatory - the value of billings</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pay_status",
            "description": "<p>Mandatory - the pay status of billings</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "late_pay",
            "description": "<p>Mandatory - the late pay of billings</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "BAD_REQUEST",
            "description": "<p>dont have anythings</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "201": [
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>billings created.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.client",
            "description": "<p>client of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.plan",
            "description": "<p>plan of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.month",
            "description": "<p>month of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.year",
            "description": "<p>year of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.value",
            "description": "<p>value of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.pay_status",
            "description": "<p>pay status of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.late_pay",
            "description": "<p>late pay of created billings.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Billings.js",
    "groupTitle": "billingss"
  },
  {
    "type": "delete",
    "url": "/billingss/:no",
    "title": "Delete an billings with a specify no.",
    "name": "DeleteBillings",
    "group": "billingss",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "no",
            "description": "<p>Mandatory - the no of billings you want to delete.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "204": [
          {
            "group": "204",
            "type": "Vono",
            "optional": false,
            "field": "response",
            "description": "<p>The vono if the action works.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Billings.js",
    "groupTitle": "billingss"
  },
  {
    "type": "get",
    "url": "/billingss",
    "title": "Get the list of billingss",
    "name": "GetBillings",
    "group": "billingss",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object[]",
            "optional": false,
            "field": "response",
            "description": "<p>A list of billingss.</p>"
          }
        ],
        "201": [
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.client",
            "description": "<p>client of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.plan",
            "description": "<p>plan of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.month",
            "description": "<p>month of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.year",
            "description": "<p>year of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.value",
            "description": "<p>value of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.pay_status",
            "description": "<p>pay status of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.late_pay",
            "description": "<p>late pay of created billings.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Billings.js",
    "groupTitle": "billingss"
  },
  {
    "type": "get",
    "url": "/billingss/client/:clientRUT",
    "title": "Get an billings with a specify client.",
    "name": "GetBillings",
    "group": "billingss",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "RUT",
            "description": "<p>Mandatory - the RUT of client you want to get.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>A list of billingss.</p>"
          }
        ],
        "201": [
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.client",
            "description": "<p>client of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.plan",
            "description": "<p>plan of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.month",
            "description": "<p>month of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.year",
            "description": "<p>year of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.value",
            "description": "<p>value of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.pay_status",
            "description": "<p>pay status of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.late_pay",
            "description": "<p>late pay of created billings.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Billings.js",
    "groupTitle": "billingss"
  },
  {
    "type": "get",
    "url": "/billingss/plan/:planName",
    "title": "Get an billings with a specify no.",
    "name": "GetBillings",
    "group": "billingss",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory - the name of the plan you want to get.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>A list of billingss.</p>"
          }
        ],
        "201": [
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.client",
            "description": "<p>client of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.plan",
            "description": "<p>plan of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.month",
            "description": "<p>month of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.year",
            "description": "<p>year of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.value",
            "description": "<p>value of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.pay_status",
            "description": "<p>pay status of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.late_pay",
            "description": "<p>late pay of created billings.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Billings.js",
    "groupTitle": "billingss"
  },
  {
    "type": "get",
    "url": "/billingss/:no",
    "title": "Get an billings with a specify no.",
    "name": "GetBillings",
    "group": "billingss",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "no",
            "description": "<p>Mandatory - the no of billings you want to get.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "response",
            "description": "<p>A list of billingss.</p>"
          }
        ],
        "201": [
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.client",
            "description": "<p>client of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.plan",
            "description": "<p>plan of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.month",
            "description": "<p>month of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.year",
            "description": "<p>year of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.value",
            "description": "<p>value of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.pay_status",
            "description": "<p>pay status of created billings.</p>"
          },
          {
            "group": "201",
            "type": "Object",
            "optional": false,
            "field": "response.late_pay",
            "description": "<p>late pay of created billings.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Billings.js",
    "groupTitle": "billingss"
  },
  {
    "type": "patch",
    "url": "/billingss/:no",
    "title": "Update an billings with a specify no.",
    "name": "UpdateBillings",
    "group": "billingss",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "no",
            "description": "<p>Mandatory - the no of billings you want to update.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "204": [
          {
            "group": "204",
            "type": "Vono",
            "optional": false,
            "field": "response",
            "description": "<p>The vono if the action works.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./controllers/Billings.js",
    "groupTitle": "billingss"
  }
] });
