class Node { // Vamos a index por price
    values = [];     // Representa los vehicles que cumplen con las caracteristicas de price
    // Si 6 vehicles tienen 4 price, lo logico es que todos los vehicles de 4 price queden dentro del arreglo valores
    nodevalue = 0;
    left = null;
    right = null;

    constructor(nodevalue) {
        this.nodevalue = nodevalue;
    }

    index(object, value) {
        if (value === this.nodevalue) {
            this.values.push(object)
        } else if (value < this.nodevalue) {
            if (this.left) {
                this.left.index(object, value);
            } else {
                this.left = new Node(value);
                this.left.index(object, value);
            }
        } else if (value > this.nodevalue) {
            if (this.right) {
                this.right.index(object, value);
            } else {
                this.right = new Node(value);
                this.right.index(object, value);
            }
        }
    }

    search(value) {
        if (value === this.nodevalue) {
            return this.values;
        } else if (value < this.nodevalue) {
            if (this.left) {
                return this.left.search(value);
            } else {
                // ESTO QUIERE DECIR QUE No se han encontrado resultados EN EL ARBOL CON LOS PARAMETROS QUE ESTAMOS BUSCANDO
                alert('No se han encontrado resultados');
                return [];
            }

        } else if (value > this.nodevalue) {
            if (this.right) {
                return this.right.search(value);
            } else {
                // ESTO QUIERE DECIR QUE No se han encontrado resultados EN EL ARBOL CON LOS PARAMETROS QUE ESTAMOS BUSCANDO
                alert('No se han encontrado resultados');
                return [];
            }

        }
    }
}

class Tree {
    indice = null;

    index(object, value) {
        if (this.indice) {
            this.indice.index(object, value);
        } else {
            this.indice = new Node(value);
            this.indice.index(object, value);
        }
    }

    find(value) {
        if (this.indice) {
            return this.indice.search(value);
        } else {
            alert('No se han encontrado resultados');
            return [];
        }
    }
}

class Searcher {

    price = new Tree();
    model = new Tree();
    brand = new Tree();
    colour = new Tree();
    cc = new Tree();
    type = new Tree();

    constructor() {
        this.index();
    }

    index() {
        // la constante vehicles viene del archivo de moockaroo importado como anilaes.js en una constante global llamada vehicles
    }

    add(vehicles) {
        const vehicle = vehicles;
        this.price.index(vehicle, vehicles[0]);
        this.model.index(vehicle, vehicles[0]);
        this.brand.index(vehicle, vehicles[0]);
        this.colour.index(vehicle, vehicles[0]);
        this.cc.index(vehicle, vehicles[0]);
        this.type.index(vehicle, vehicles[0]);
    }
}

let searcher = new Searcher();
searcher.add([666999, "toyora", "tesla", "Red", 0, 2]);
searcher.add([666999, "usu", "tesla", "Blue", 0, 2]);
searcher.add([666999, "komatsu", "tesla", "Green", 0, 2]);
searcher.add([888, "aaa", "tes", "Black", 0, 2]);
console.log(price);