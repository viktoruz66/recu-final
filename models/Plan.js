class Plan {
  name = null;
  trans_speed = null;
  plan_type = null;
  promotion = null;
  value = null;

  constructor(plan = null) {
    if (plan) {
      this.name = plan.name;
      this.trans_speed = plan.trans_speed;
      this.plan_type = plan.plan_type;
      this.promotion = plan.promotion;
      this.value = plan.value;
    }
  }
}
module.exports = Plan;