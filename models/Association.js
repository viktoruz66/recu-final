class Association {
  client = null;
  plan = null;
  assign = null;
  adress = null;
  begin_date = null;

  constructor(association = null) {
    if (association) {
      this.client = association.client;
      this.plan = association.plan;
      this.assign = association.assign;
      this.adress = association.adress;
      this.begin_date = association.begin_date;
    }
  }
}
module.exports = Association;