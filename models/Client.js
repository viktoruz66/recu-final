class Client {
  name = null;
  last_name = null;
  RUT = null;
  birth_date = null;
  phone = null;
  adress = null;
  genre = null;

  constructor(client = null) {
    if (client) {
      this.name = client.name;
      this.last_name = client.last_name;
      this.RUT = client.RUT;
      this.birth_date = client.birth_date;
      this.phone = client.phone;
      this.adress = client.adress;
      this.genre = client.genre;
    }
  }
}
module.exports = Client;