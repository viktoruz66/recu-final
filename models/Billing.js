class Billing {
  no = null;
  IVA = (0.19);
  client = null;
  plan = null;
  month = null;
  year = null;
  value = null;
  pay_status = null;
  late_pay = null;

  constructor(billing = null) {
    if (billing) {
      this.no = billing.no;
      this.client = billing.client;
      this.plan = billing.plan;
      this.month = billing.month;
      this.year = billing.year;
      this.value = billing.value;
      this.pay_status = billing.pay_status;
      this.late_pay = billing.late_pay;
    }
  }
}
module.exports = Billing; 