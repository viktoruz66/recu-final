const ClientsController = require('./controllers/Clients');
const PlansController = require('./controllers/Plans');
const AssociationsController = require('./controllers/Associations');
const BillingsController = require('./controllers/Billings');

const bodyParser = require('body-parser');

var express = require('express');
var app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

new ClientsController(app);
new PlansController(app);
new AssociationsController(app);
new BillingsController(app);

app.get('/', function (req, res) {
  res.send("library inventory's API");
});

app.post('/', function (req, res) {
  res.send('Api of library');
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
