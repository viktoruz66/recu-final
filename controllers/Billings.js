const Billing = require('../models/Billing');
let fs = require('fs');
billings = [];

/**
 * @autor Flores, Victor
 * this class have the controller we need to do
 * the CRUD operations from the Billings
 */

class BillingsController {
    path = 'billings'
    no = 0;
    constructor(app) {
        this.router(app);
        //        this.indexing();
        console.log('endpoint Billings initialized');
    }

    router(app) {
        var self = this;
        // Create
        app.post(`/${this.path}`, function (req, res) {
            self.create(req, res);
        });
        // Read
        app.get(`/${this.path}`, function (req, res) {
            self.read(req, res);
        });
        // Read Client
        app.get(`/${this.path}/client/:clientRUT`, function (req, res) {
            self.readOne(req, res);
        });
        // Read Plan
        app.get(`/${this.path}/plan/:planName`, function (req, res) {
            self.readOne(req, res);
        });
        // Read No
        app.get(`/${this.path}/:no`, function (req, res) {
            self.readOne(req, res);
        });
        // Update
        app.patch(`/${this.path}/:assign`, function (req, res) {
            self.update(req, res);
        });
        // Delete
        app.delete(`/${this.path}/:assign`, function (req, res) {
            self.destroy(req, res);
        });
    }

    /**
    * @api {post} /billingss Create a new billings
    * @apiName CreateBillings
    * @apiGroup billingss
    * 
    * @apiParam {String} client Mandatory - the client of billings
    * @apiParam {Number} plan Mandatory - the plan of billings
    * @apiParam {Number} month Mandatory - the month of billings
    * @apiParam {Number} year Mandatory - the year of billings
    * @apiParam {Number} value Mandatory - the value of billings
    * @apiParam {Number} pay_status Mandatory - the pay status of billings
    * @apiParam {Number} late_pay Mandatory - the late pay of billings
    * 
    * @apiError (400) BAD_REQUEST dont have anythings
    * 
    * @apiSuccess (201) {Object} response billings created.
    * @apiSuccess (201) {Object} response.client client of created billings.
    * @apiSuccess (201) {Object} response.plan plan of created billings.
    * @apiSuccess (201) {Object} response.month month of created billings.
    * @apiSuccess (201) {Object} response.year year of created billings.
    * @apiSuccess (201) {Object} response.value value of created billings.
    * @apiSuccess (201) {Object} response.pay_status pay status of created billings.
    * @apiSuccess (201) {Object} response.late_pay late pay of created billings.
    * 
    **/

    create(req, res) {
        const clientRUT = req.body.clientRUT;
        const planName = req.body.planName;
        const month = req.body.month;
        const year = req.body.year;
        const value = req.body.value;
        const pay_status = req.body.pay_status;
        const late_pay = req.body.late_pay;
        var client = null;
        var plan = null;

        if (!(clientRUT && planName && month && year && value && pay_status
            && late_pay)) {
            res.status(400);
            return res.send('Datos incompletos');
        }
        for (let i = 0; i < clients.length; i++) {
            if (clients[i].RUT === clientRUT) {
                client = clients[i];
            }
        }

        if (!client) {
            return res.send('Client does not exists')
        }

        for (let i = 0; i < plans.length; i++) {
            if (plans[i].name === planName) {
                plan = plans[i];
            }
        }

        if (!plan) {
            return res.send('Plan does not exists')
        }

        const billing = new Billing(
            {
                client: client,
                plan: plan,
                month: month,
                year: year,
                value: value,
                pay_status: pay_status,
                late_pay: late_pay
            }
        )
        billings.push(billing);
        res.status(201);
        res.send(
            { ...billing }
        );
    }

    /**
    * @api {get} /billingss Get the list of billingss
    * @apiName GetBillings
    * @apiGroup billingss
    * 
    * @apiSuccess (200) {Object[]} response A list of billingss.
    * @apiSuccess (201) {Object} response.client client of created billings.
    * @apiSuccess (201) {Object} response.plan plan of created billings.
    * @apiSuccess (201) {Object} response.month month of created billings.
    * @apiSuccess (201) {Object} response.year year of created billings.
    * @apiSuccess (201) {Object} response.value value of created billings.
    * @apiSuccess (201) {Object} response.pay_status pay status of created billings.
    * @apiSuccess (201) {Object} response.late_pay late pay of created billings.
    * 
    **/
    read(req, res) {
        res.status(501);
        res.send(billings);
    }
    /**
    * @api {get} /billingss/client/:clientRUT Get an billings with a specify client.
    * @apiName GetBillings
    * @apiGroup billingss
    * 
    * @apiParam {String} RUT Mandatory - the RUT of client you want to get.
    * 
    * @apiSuccess (200) {Object} response A list of billingss.
    * @apiSuccess (201) {Object} response.client client of created billings.
    * @apiSuccess (201) {Object} response.plan plan of created billings.
    * @apiSuccess (201) {Object} response.month month of created billings.
    * @apiSuccess (201) {Object} response.year year of created billings.
    * @apiSuccess (201) {Object} response.value value of created billings.
    * @apiSuccess (201) {Object} response.pay_status pay status of created billings.
    * @apiSuccess (201) {Object} response.late_pay late pay of created billings.
    * 
    **/

    readClient(req, res) {
        let RUT = req.params.clientRUT;
        let coincidences = [];
        for (let i = 0; i < associations.length; i++) {
            if (associations[i].client.RUT === RUT) {
                coincidences.push(associations[i]);
            }
        }
        res.status(200);
        res.send(coincidences);
    }

    /**
    * @api {get} /billingss/plan/:planName Get an billings with a specify no.
    * @apiName GetBillings
    * @apiGroup billingss
    * 
    * @apiParam {String} name Mandatory - the name of the plan you want to get.
    * 
    * @apiSuccess (200) {Object} response A list of billingss.
    * @apiSuccess (201) {Object} response.client client of created billings.
    * @apiSuccess (201) {Object} response.plan plan of created billings.
    * @apiSuccess (201) {Object} response.month month of created billings.
    * @apiSuccess (201) {Object} response.year year of created billings.
    * @apiSuccess (201) {Object} response.value value of created billings.
    * @apiSuccess (201) {Object} response.pay_status pay status of created billings.
    * @apiSuccess (201) {Object} response.late_pay late pay of created billings.
    * 
    **/

    readPlan(req, res) {
        let name = req.params.planName;
        let coincidences = [];
        for (let i = 0; i < associations.length; i++) {
            if (associations[i].plan.name === name) {
                coincidences.push(associations[i]);
            }
        }
        res.status(200);
        res.send(coincidences);
    }

    /**
    * @api {get} /billingss/:no Get an billings with a specify no.
    * @apiName GetBillings
    * @apiGroup billingss
    * 
    * @apiParam {Number} no Mandatory - the no of billings you want to get.
    * 
    * @apiSuccess (200) {Object} response A list of billingss.
    * @apiSuccess (201) {Object} response.client client of created billings.
    * @apiSuccess (201) {Object} response.plan plan of created billings.
    * @apiSuccess (201) {Object} response.month month of created billings.
    * @apiSuccess (201) {Object} response.year year of created billings.
    * @apiSuccess (201) {Object} response.value value of created billings.
    * @apiSuccess (201) {Object} response.pay_status pay status of created billings.
    * @apiSuccess (201) {Object} response.late_pay late pay of created billings.
    * 
    **/

    readOne(req, res) {
        let assign = req.params.assign;
        let coincidences = [];
        for (let i = 0; i < associations.length; i++) {
            if (associations[i].assign === assign) {
                coincidences.push(associations[i]);
            }
        }
        res.status(200);
        res.send(coincidences);
    }

    /**
    * @api {patch} /billingss/:no Update an billings with a specify no.
    * @apiName UpdateBillings
    * @apiGroup billingss
    * 
    * @apiParam {Number} no Mandatory - the no of billings you want to update.
    * 
     * @apiSuccess (204) {Vono} response The vono if the action works.
    * 
    **/

    update(req, res) {
        res.status(204);
        res.send();
    }

    /**
    * @api {delete} /billingss/:no Delete an billings with a specify no.
    * @apiName DeleteBillings
    * @apiGroup billingss
    * 
    * @apiParam {Number} no Mandatory - the no of billings you want to delete.
    * 
    * @apiSuccess (204) {Vono} response The vono if the action works.
    * 
    **/

    destroy(req, res) {
        res.status(204);
        res.send();
    }

}
module.exports = BillingsController;