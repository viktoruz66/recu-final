const Client = require('../models/Client');
let fs = require('fs');
clients = [];

/**
 * @autor Flores, Victor
 * this class have the controller we need to do
 * the CRUD operations from the Clients
 */

class ClientsController {
    path = 'clients'
    RUT = 0;
    constructor(app) {
        this.router(app);
        this.indexing();
        console.log('endpoint Clients initialized');
    }

    router(app) {
        var self = this;
        // Create
        app.post(`/${this.path}`, function (req, res) {
            self.create(req, res);
        });
        // Read
        app.get(`/${this.path}`, function (req, res) {
            self.read(req, res);
        });
        // Read RUT
        app.get(`/${this.path}/rut/:RUT`, function (req, res) {
            self.readRUT(req, res);
        });
        // Read name
        app.get(`/${this.path}/lastname/:last_name`, function (req, res) {
            self.readName(req, res);
        });
        // Update
        app.patch(`/${this.path}/rut/:RUT`, function (req, res) {
            self.update(req, res);
        });
        // Delete
        app.delete(`/${this.path}/rut/:RUT`, function (req, res) {
            self.destroy(req, res);
        });
    }

    /**
    * @api {post} /clients Create a new client
    * @apiName CreateClient
    * @apiGroup Clients
    * 
    * @apiParam {String} name Mandatory - the name of client
    * @apiParam {String} last_name Mandatory - the last name of client
    * @apiParam {String} RUT Mandatory - the identifier of client
    * @apiParam {Date} birth_date Mandatory - the birth date of client
    * @apiParam {Number} phone Mandatory - the phone of client
    * @apiParam {String} adress Mandatory - the adress date of client
    * @apiParam {Char} genre Mandatory - the genre of client
    * 
    * @apiError (400) BAD_REQUEST dont have anythings
    * 
    * @apiSuccess (201) {Object} response client created.
    * @apiSuccess (201) {Object} response.RUT RUT of created client.
    * @apiSuccess (201) {Object} response.name name of created client.
    * @apiSuccess (201) {Object} response.last_name last name of created client.
    * @apiSuccess (201) {Object} response.RUT identifier of created client.
    * @apiSuccess (201) {Object} response.birth_date birth date of created client.
    * @apiSuccess (201) {Object} response.phone phone of created client.
    * @apiSuccess (201) {Object} response.adress adress of created client.
    * @apiSuccess (201) {Object} response.genre genre of created client.
    * 
    **/

    create(req, res) {
        const name = req.body.name;
        const last_name = req.body.last_name;
        const RUT = req.body.RUT;
        const birth_date = req.body.birth_date;
        const phone = req.body.phone;
        const adress = req.body.adress;
        const genre = req.body.genre;

        if (!(name && last_name && RUT && birth_date && phone && adress
            && genre)) {
            res.status(400);
            return res.send('Datos incompletos');
        }

        for (let i = 0; i < clients.length; i++) {
            if (clients[i].RUT === RUT) {
                res.status(400);
                return res.send('No se admite un rut que ya exista');
            }
        }

        res.status(201);
        const client = new Client(
            {
                name: name,
                last_name: last_name,
                RUT: RUT,
                birth_date: birth_date,
                phone: phone,
                adress: adress,
                genre: genre
            }
        )
        clients.push(client);
        res.send(
            { ...client }
        );
    }

    /**
    * @api {get} /clients Get the list of clients
    * @apiName GetClient
    * @apiGroup Clients
    * 
    * @apiSuccess (200) {Object[]} response A list of clients.
    * @apiSuccess (200) {Object} response.RUT RUT of client.
    * @apiSuccess (200) {Object} response.name name of client.
    * @apiSuccess (200) {Object} response.last_name last name of client.
    * @apiSuccess (200) {Object} response.RUT RUT of client.
    * @apiSuccess (200) {Object} response.birth_date birth date of client.
    * @apiSuccess (200) {Object} response.phone phone of client.
    * @apiSuccess (200) {Object} response.adress adress of created client.
    * @apiSuccess (200) {Object} response.genre genre of created client.
    * 
    **/
    read(req, res) {
        res.status(200);
        res.send(clients);
    }

    /**
    * @api {get} /clients/rut/:RUT Get an client with a specify identifier.
    * @apiName GetClient
    * @apiGroup Clients
    * 
    * @apiParam {String} RUT Mandatory - the identifier of client you want to get.
    * 
    * @apiSuccess (200) {Object} response A list of clients.
    * @apiSuccess (200) {Object} response.RUT RUT of client.
    * @apiSuccess (200) {Object} response.name name of client.
    * @apiSuccess (200) {Object} response.last_name last name of client.
    * @apiSuccess (200) {Object} response.RUT RUT of client.
    * @apiSuccess (200) {Object} response.birth date birth_date of client.
    * @apiSuccess (200) {Object} response.phone phone of client.
    * @apiSuccess (200) {Object} response.adress adress of created client.
    * @apiSuccess (200) {Object} response.genre genre of created client.
    * 
    **/

    readRUT(req, res) {
        let RUT = req.params.RUT;
        let coincidences = [];
        for (let i = 0; i < clients.length; i++) {
            if (clients[i].RUT === RUT) {
                coincidences.push(clients[i]);
            }
        }
        res.status(200);
        res.send(coincidences);
    }

    /**
    * @api {get} /clients/lastname/:last_name Get an client with a specify last name.
    * @apiName GetClient
    * @apiGroup Clients
    * 
    * @apiParam {String} last_name Mandatory - the last name of client you want to get.
    * 
    * @apiSuccess (200) {Object} response A list of clients.
    * @apiSuccess (200) {Object} response.RUT RUT of client.
    * @apiSuccess (200) {Object} response.name name of client.
    * @apiSuccess (200) {Object} response.last_name last name of client.
    * @apiSuccess (200) {Object} response.RUT RUT of client.
    * @apiSuccess (200) {Object} response.birth_date birth date of client.
    * @apiSuccess (200) {Object} response.phone phone of client.
    * @apiSuccess (200) {Object} response.adress adress of created client.
    * @apiSuccess (200) {Object} response.genre genre of created client.
    * 
    **/
    readName(req, res) {
        let last_name = req.params.last_name;
        let coincidences = [];
        for (let i = 0; i < clients.length; i++) {
            if (clients[i].last_name === last_name) {
                coincidences.push(clients[i]);
            }
        }
        res.status(200);
        res.send(coincidences);
    }

    /**
    * @api {patch} /clients/rut/:RUT Update an client with a specify identifier.
    * @apiName UpdateClient
    * @apiGroup Clients
    * 
    * @apiParam {String} RUT Mandatory - the RUT of client you want to update
    * @apiParam {String} name Mandatory - the name of client
    * @apiParam {String} last_name Mandatory - the last name of client
    * @apiParam {Date} birth_date Mandatory - the birth date of client
    * @apiParam {Number} phone Mandatory - the phone of client
    * @apiParam {String} adress Mandatory - the adress date of client
    * @apiParam {Char} genre Mandatory - the genre of client
    * 
    * @apiError (400) BAD_REQUEST dont have anythings
    * @apiSuccess (204) {VoRUT} response The voRUT if the action works.
    * 
    **/

    update(req, res) {
        const name = req.body.name;
        const last_name = req.body.last_name;
        const RUT = req.params.RUT;
        const birth_date = req.body.birth_date;
        const phone = req.body.phone;
        const adress = req.body.adress;
        const genre = req.body.genre;
        let coincidence;

        if (!(name && last_name && RUT && birth_date && phone && adress
            && genre)) {
            res.status(400);
            return res.send('Incomplete data');
        }

        for (let i = 0; i < clients.length; i++) {
            if (clients[i].RUT === RUT) {
                coincidence = clients[i];
                clients.splice(i, 1);
            }
        }

        const client = new Client(
            {
                name: name,
                last_name: last_name,
                RUT: coincidence.RUT,
                birth_date: birth_date,
                phone: phone,
                adress: adress,
                genre: genre
            }
        )

        clients.push(client);
        res.status(204);
        res.send('Client patched');
    }

    /**
    * @api {delete} /clients/rut/:RUT Delete an client with a specify RUT.
    * @apiName DeleteClient
    * @apiGroup Clients
    * 
    * @apiParam {Number} RUT Mandatory - the RUT of client you want to delete.
    * @apiSuccess (204) {VoRUT} response The voRUT if the action works.
    * 
    **/

    destroy(req, res) {
        const RUT = req.params.RUT;

        for (let i = 0; i < clients.length; i++) {
            if (clients[i].RUT === RUT) {
                clients.splice(i, 1);
            }
        }
        res.status(204);
        res.send('Client deleted');
    }

    /**
    * El siguiente metodo agrega los datos disponibles en MOCK_Clients.json
    **/
    indexing() {
        clients = JSON.parse(fs.readFileSync('./MOCK/MOCK_Clients.json', 'utf-8'));
    }
}
module.exports = ClientsController;