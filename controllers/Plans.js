const Plan = require('../models/Plan');
let fs = require('fs');
plans = [];

/**
 * @autor Flores, Victor
 * this class have the controller we need to do
 * the CRUD operations from the Plans
 */

class PlansController {
    path = 'plans'
    name = 0;
    constructor(app) {
        this.router(app);
        this.indexing();
        console.log('endpoint Plan initialized');
    }

    router(app) {
        var self = this;
        // Create
        app.post(`/${this.path}`, function (req, res) {
            self.create(req, res);
        });
        // Read
        app.get(`/${this.path}`, function (req, res) {
            self.read(req, res);
        });
        // Read One
        app.get(`/${this.path}/:name`, function (req, res) {
            self.readOne(req, res);
        });
        // Update
        app.patch(`/${this.path}/:name`, function (req, res) {
            self.update(req, res);
        });
        // Delete
        app.delete(`/${this.path}/:name`, function (req, res) {
            self.destroy(req, res);
        });
    }

    /**
    * @api {post} /plans Create a new plan
    * @apiName CreatePlan
    * @apiGroup Plans
    * 
    * @apiParam {String} name Mandatory - the name of plan
    * @apiParam {Number} trans_speed Mandatory - the trans speed of plan
    * @apiParam {Boolean} plan_type Mandatory - the plan type of plan
    * @apiParam {Boolean} promotion Mandatory - the promotion of plan
    * @apiParam {Number} value Mandatory - the value of plan
    * 
    * @apiError (400) BAD_REQUEST dont have anythings
    * 
    * @apiSuccess (201) {Object} response plan created.
    * @apiSuccess (201) {Object} response.name name of created plan.
    * @apiSuccess (201) {Object} response.trans_speed trans speed of created plan.
    * @apiSuccess (201) {Object} response.plan_type plan type of created plan.
    * @apiSuccess (201) {Object} response.promotion promotion of created plan.
    * @apiSuccess (201) {Object} response.value value of created plan.
    * 
    **/

    create(req, res) {
        const name = req.body.name;
        const trans_speed = req.body.trans_speed;
        const plan_type = req.body.plan_type;
        const promotion = req.body.promotion;
        const value = req.body.value;

        if (!(name && trans_speed && plan_type && promotion && value)) {
            res.status(400);
            return res.send('Datos incompletos');
        }
        res.status(201);
        const plan = new Plan(
            {
                name: name,
                trans_speed: trans_speed,
                plan_type: plan_type,
                promotion: promotion,
                value: value
            }
        )
        plans.push(plan);
        res.send(
            { ...plan }
        );
    }

    /**
    * @api {get} /plans Get the list of plans
    * @apiName GetPlan
    * @apiGroup Plans
    * 
    * @apiSuccess (200) {Object[]} response A list of plans.
    * @apiSuccess (200) {Object} response.name name of plan.
    * @apiSuccess (200) {Object} response.trans_speed trans speed of plan.
    * @apiSuccess (200) {Object} response.plan_type plan type of plan.
    * @apiSuccess (200) {Object} response.promotion promotion of plan.
    * @apiSuccess (200) {Object} response.value value of plan.
    * 
    **/
    read(req, res) {
        res.status(200);
        res.send(plans);
    }

    /**
    * @api {get} /plans/:name Get an plan with a specify name.
    * @apiName GetPlan
    * @apiGroup Plans
    * 
    * @apiParam {String} name Mandatory - the name of plan you want to get.
    * 
    * @apiSuccess (200) {Object} response A list of plans.
    * @apiSuccess (200) {Object} response.name name of plan.
    * @apiSuccess (200) {Object} response.trans_speed trans speed of plan.
    * @apiSuccess (200) {Object} response.plan_type plan type of plan.
    * @apiSuccess (200) {Object} response.promotion promotion of plan.
    * @apiSuccess (200) {Object} response.value value of plan.
    * 
    **/

    readOne(req, res) {
        let name = req.params.name;
        let coincidences = [];
        for (let i = 0; i < plans.length; i++) {
            if (plans[i].name === name) {
                coincidences.push(plans[i]);
            }
        }
        res.status(200);
        res.send(coincidences);
    }

    /**
    * @api {patch} /plans/:name Update an plan with a specify name.
    * @apiName UpdatePlan
    * @apiGroup Plans
    * 
    * @apiParam {String} name Mandatory - the name of plan you want to update.
    * @apiParam {Number} trans_speed Mandatory - the trans speed of plan
    * @apiParam {Boolean} plan_type Mandatory - the plan type of plan
    * @apiParam {Boolean} promotion Mandatory - the promotion of plan
    * @apiParam {Number} value Mandatory - the value of plan
    * 
    * @apiError (400) BAD_REQUEST dont have anythings
    * @apiSuccess (204) {VoName} response The voname if the action works.
    * 
    **/

    update(req, res) {
        const name = req.params.name;
        const trans_speed = req.body.trans_speed;
        const plan_type = req.body.plan_type;
        const promotion = req.body.promotion;
        const value = req.body.value;

        if (!(trans_speed && plan_type && promotion && value)) {
            res.status(400);
            return res.send('Datos incompletos');
        }

        for (let i = 0; i < plans.length; i++) {
            if (plans[i].name === name) {
                plans.splice(i, 1);

            }
        }

        const plan = new Plan(
            {
                name: name,
                trans_speed: trans_speed,
                plan_type: plan_type,
                promotion: promotion,
                value: value
            }
        )
        plans.push(plan);
        res.status(204);
        res.send('Plan patched')
    }

    /**
    * @api {delete} /plans/:name Delete an plan with a specify name.
    * @apiName DeletePlan
    * @apiGroup Plans
    * 
    * @apiParam {String} name Mandatory - the name of plan you want to delete.
    * @apiSuccess (204) {Voname} response The voname if the action works.
    * 
    **/

    destroy(req, res) {
        const name = req.params.name;

        for (let i = 0; i < plans.length; i++) {
            if (plans[i].name === name) {
                plans.splice(i, 1);
            }
        }

        res.status(204);
        res.send('Plan deleted');
    }

    /**
    * El siguiente metodo agrega los datos disponibles en MOCK_Clients.json
    **/
    indexing() {
        plans = JSON.parse(fs.readFileSync('./MOCK/MOCK_Plans.json', 'utf-8'));
    }
}
module.exports = PlansController;