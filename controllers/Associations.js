const Association = require('../models/Association');
let fs = require('fs');
associations = [];
let today = new Date().getDate() + '/' + (new Date().getMonth() + 1) + '/'
    + new Date().getFullYear();

/**
 * @autor Flores, Victor
 * this class have the controller we need to do
 * the CRUD operations from the Associations
 */

class AssociationsController {
    path = 'associations'
    assign = 0;
    constructor(app) {
        this.router(app);
        //        this.indexing();
        console.log('endpoint Associations initialized');
    }

    router(app) {
        var self = this;
        // Create
        app.post(`/${this.path}`, function (req, res) {
            self.create(req, res);
        });
        // Read
        app.get(`/${this.path}`, function (req, res) {
            self.read(req, res);
        });
        // Read Client
        app.get(`/${this.path}/client/:clientRUT`, function (req, res) {
            self.readClient(req, res);
        });
        // Read Plan
        app.get(`/${this.path}/plan/:planName`, function (req, res) {
            self.readPlan(req, res);
        });
        // Read Assign
        app.get(`/${this.path}/:assign`, function (req, res) {
            self.readOne(req, res);
        });
        // Update
        app.patch(`/${this.path}/:assign`, function (req, res) {
            self.update(req, res);
        });
        // Delete
        app.delete(`/${this.path}/:assign`, function (req, res) {
            self.destroy(req, res);
        });
    }

    /**
    * @api {post} /associations Create a new association
    * @apiName CreateAssociation
    * @apiGroup associations
    * 
    * @apiParam {ClientRUT} client Mandatory - the client of association
    * @apiParam {PlanName} plan Mandatory - the plan of association
    * @apiParam {Number} assign Mandatory - the assign of association
    * @apiParam {String} adress Mandatory - the adress of association
    * @apiParam {Date} begin_date Mandatory - the begin date of association
    * 
    * @apiError (400) BAD_REQUEST dont have anythings
    * 
    * @apiSuccess (201) {Object} response association created.
    * @apiSuccess (201) {Object} response.client client of created association.
    * @apiSuccess (201) {Object} response.plan plan of created association.
    * @apiSuccess (201) {Object} response.assign assign of created association.
    * @apiSuccess (201) {Object} response.adress adress of created association.
    * @apiSuccess (201) {Object} response.begin_date begin date of created association.
    * 
    **/

    create(req, res) {
        const clientRUT = req.body.clientRUT;
        const planName = req.body.planName;
        const assign = req.body.assign;
        const adress = req.body.adress;
        var client = null;
        var plan = null;

        if (!(clientRUT && planName && assign && adress)) {
            res.status(400);
            return res.send('Incomplete data');
        }

        for (let i = 0; i < clients.length; i++) {
            if (clients[i].RUT === clientRUT) {
                client = clients[i];
            }
        }

        if (!client) {
            return res.send('Client does not exists')
        }

        for (let i = 0; i < plans.length; i++) {
            if (plans[i].name === planName) {
                plan = plans[i];
            }
        }

        if (!plan) {
            return res.send('Plan does not exists')
        }

        const association = new Association(
            {
                client: client,
                plan: plan,
                assign: assign,
                adress: adress,
                begin_date: today
            }
        )
        associations.push(association);
        res.status(201);
        res.send(
            { ...association }
        );
    }

    /**
    * @api {get} /associations Get the list of associations
    * @apiName GetAssociation
    * @apiGroup associations
    * 
    * @apiSuccess (200) {Object[]} response A list of associations.
    * @apiSuccess (200) {Object} response.client client of association.
    * @apiSuccess (200) {Object} response.plan plan of association.
    * @apiSuccess (200) {Object} response.assign assign of association.
    * @apiSuccess (200) {Object} response.adress adress of association.
    * @apiSuccess (200) {Object} response.begin_date begin date of association.
    * 
    **/
    read(req, res) {
        res.status(200);
        res.send(associations);
    }

    /**
    * @api {get} /associations/client/:clientRUT Get an association with a specify client.
    * @apiName GetAssociation
    * @apiGroup associations
    * 
    * @apiParam {String} RUT Mandatory - the RUT of client you want to get.
    * 
    * @apiSuccess (200) {Object} response A list of associations.
    * 
    * @apiSuccess (200) {Object} response.client client of association.
    * @apiSuccess (200) {Object} response.plan plan of association.
    * @apiSuccess (200) {Object} response.assign assign of association.
    * @apiSuccess (200) {Object} response.adress adress of association.
    * @apiSuccess (200) {Object} response.begin_date begin date of association.
    * 
    **/
    readClient(req, res) {
        let RUT = req.params.clientRUT;
        let coincidences = [];
        for (let i = 0; i < associations.length; i++) {
            if (associations[i].client.RUT === RUT) {
                coincidences.push(associations[i]);
            }
        }
        res.status(200);
        res.send(coincidences);
    }

    /**
    * @api {get} /associations/plan/:planName Get an association with a specify plan.
    * @apiName GetAssociation
    * @apiGroup associations
    * 
    * @apiParam {String} name Mandatory - the name of the plan you want to get.
    * 
    * @apiSuccess (200) {Object} response A list of associations.
    * 
    * @apiSuccess (200) {Object} response.client client of association.
    * @apiSuccess (200) {Object} response.plan plan of association.
    * @apiSuccess (200) {Object} response.assign assign of association.
    * @apiSuccess (200) {Object} response.adress adress of association.
    * @apiSuccess (200) {Object} response.begin_date begin date of association.
    * 
    **/
    readPlan(req, res) {
        let name = req.params.planName;
        let coincidences = [];
        for (let i = 0; i < associations.length; i++) {
            if (associations[i].plan.name === name) {
                coincidences.push(associations[i]);
            }
        }
        res.status(200);
        res.send(coincidences);
    }

    /**
    * @api {get} /associations/:assign Get an association with a specify assign.
    * @apiName GetAssociation
    * @apiGroup associations
    * 
    * @apiParam {Number} assign Mandatory - the assign of association you want to get.
    * 
    * @apiSuccess (200) {Object} response A list of associations.
    * 
    * @apiSuccess (200) {Object} response.client client of association.
    * @apiSuccess (200) {Object} response.plan plan of association.
    * @apiSuccess (200) {Object} response.assign assign of association.
    * @apiSuccess (200) {Object} response.adress adress of association.
    * @apiSuccess (200) {Object} response.begin_date begin date of association.
    * 
    **/
    readOne(req, res) {
        let assign = req.params.assign;
        let coincidences = [];
        for (let i = 0; i < associations.length; i++) {
            if (associations[i].assign === assign) {
                coincidences.push(associations[i]);
            }
        }
        res.status(200);
        res.send(coincidences);
    }

    /**
    * @api {patch} /associations/:assign Update an association with a specify assign.
    * @apiName Updateassociation
    * @apiGroup associations
    * 
    * @apiParam {Number} assign Mandatory - the assign of association you want to update.
    * 
     * @apiSuccess (204) {Voassign} response The voassign if the action works.
    * 
    **/

    update(req, res) {
        res.status(204);
        res.send();
    }

    /**
    * @api {delete} /associations/:assign Delete an association with a specify assign.
    * @apiName Deleteassociation
    * @apiGroup associations
    * 
    * @apiParam {Number} assign Mandatory - the assign of association you want to delete.
    * 
    * @apiSuccess (204) {Voassign} response The voassign if the action works.
    * 
    **/

    destroy(req, res) {
        res.status(204);
        res.send();
    }

    /**
    * El siguiente metodo agrega los datos disponibles en MOCK_Clients.json
    **/
    indexing() {
        const clients = JSON.parse(fs.readFileSync(
            './MOCK/MOCK_Clients.json', 'utf-8'));
        const plans = JSON.parse(fs.readFileSync(
            './MOCK/MOCK_Plans.json', 'utf-8'));
        for (let i = 0; i < clients.length; i++) {
            let association = new Association(
                {
                    client: clients[i],
                    plan: plans[i],
                    assign: i,
                    adress: clients[i].adress,
                    begin_date: today
                }
            )
            associations.push(association);
        }

    }
}
module.exports = AssociationsController;